import networkx as nx
from timeit import default_timer as timer


def odd_verts(G):
    return [v for v in G.nodes if G.degree(v) % 2 == 1]


def req_sum(G):
    return sum([w for (u, v, w) in G.edges.data('weight')])


def fix_weights(G):
    for e in G.edges(data=True):
        e[2]['weight'] = G.graph["dist"][e[0]][e[1]]
    for v in [v for v in G.nodes if G.degree(v) == 0]:
        G.remove_node(v)


def perf_matching(G, nodes):
    assert(len(nodes) % 2 == 0)
    aux = nx.Graph()
    for u in nodes:
        for v in nodes:
            aux.add_edge(u, v, weight=-G.graph["dist"][u][v])
    return nx.max_weight_matching(aux, maxcardinality=True)


def min_connector(G):
    aux = nx.Graph()
    ccs = list(nx.connected_components(G))
    for i in range(len(ccs)):
        for j in range(i + 1, len(ccs)):
            su = sv = sw = None
            for u in ccs[i]:
                for v in ccs[j]:
                    w = G.graph['dist'][u][v]
                    if sw is None or w < sw:
                        su = u
                        sv = v
                        sw = w
            aux.add_edge(i, j, weight=sw, origu=su, origv=sv)
    return nx.minimum_spanning_tree(aux)


def bound_M(G):
    M = perf_matching(G, odd_verts(G))
    return sum([G.graph["dist"][u][v] for (u, v) in M])


def bound_T(G):
    T = min_connector(G)
    return sum([e[2]['weight'] for e in T.edges(data=True)])


def flip_balance(G, nodes, color='blue'):
    for (u, v) in perf_matching(G, nodes):
        G.add_edge(u, v, weight=G.graph["dist"][u][v], color=color)


def not_spanning(G):
    return lambda u, v, k: not G.edges[u, v, k].get('spanning', False)


def decycle(G):
    T = nx.minimum_spanning_tree(G)
    for e in T.edges:
        G.edges[e]['spanning'] = True
    subG = nx.classes.graphviews.subgraph_view(G, filter_edge=not_spanning(G))
    while True:
        try:
            C = nx.find_cycle(subG)
            G.remove_edges_from(C)
        except nx.NetworkXNoCycle:
            break


def mark_to_keep(G, eps_inv, T=0, M=0):
    for v in odd_verts(G):
        G.nodes[v]['keep'] = True

    ccs = list(nx.connected_components(G))
    C = len(ccs)

    if C == 1:
        return

    R = req_sum(G) + max(M, T, (T + M) / 2)
    gamma = R / (eps_inv * (4 * C - 4))

    for cc in ccs:
        for u in cc:
            if all([G.graph['dist'][u][v] > gamma
                    for v in cc if G.nodes[v].get('keep', False)]):
                G.nodes[u]['keep'] = True


def extract(G, v):
    if G.nodes[v].get('keep', False) or G.degree(v) == 0:
        return False

    ccs = nx.biconnected_components(G)
    cvs = nx.articulation_points(G)

    if v in cvs:
        blocks = [cc for cc in ccs if v in cc]
        if len(blocks) == 2:
            extract_two_cutvertex(G, v, blocks)
            return True
    elif len(nx.node_connected_component(G, v)) >= 3:
        extract_non_cutvertex(G, v)
        return True
    else:
        return False


def extract_non_cutvertex(G, v):
    flip_balance(G, [u for u in G.neighbors(v)
                     if G.number_of_edges(u, v) % 2 == 1])
    elist = list(G.edges(v, keys=True))
    G.remove_edges_from(elist)


def extract_two_cutvertex(G, v, blocks):
    assert(len(blocks) == 2)
    a = [e for e in G.edges(v, keys=True) if e[1] in blocks[0]][0]
    b = [e for e in G.edges(v, keys=True) if e[1] in blocks[1]][0]
    G.remove_edge(*a)
    G.remove_edge(*b)
    G.add_edge(a[1], b[1], weight=G.graph['dist'][a[1]][b[1]])
    extract_non_cutvertex(G, v)


def kernelize(G, eps_inv, T=0, M=0):
    G.graph['name'] += "k"
    t1 = timer()
    mark_to_keep(G, eps_inv, T, M)
    it=1
    while any([extract(G, v) for v in G.nodes]):
        it = it + 1
    t2 = timer()
    fix_weights(G)
    t3 = timer()
    decycle(G)
    t4 = timer()
    return (it, t2 - t1, t3 - t2, t4 - t3, t4 - t1)
