#!/bin/sh


echo "\
\\\documentclass{article}

\\\usepackage{fullpage}
\\\usepackage{graphicx}

\\\begin{document}
\\\footnotesize"


for FILE in img/*[0-9].pdf; do
    NOPDF=$(basename "$FILE" .pdf)
    echo "\\\begin{tabular}{rrrrrrrrrrrrrrrrrrrrrr}"
    grep name runs-7 | tr ',' '&'
    echo '\\\\'
    grep $NOPDF runs-7  | sed 's/_/\\textunderscore{}/g' | tr ',' '&'
    echo "\\\end{tabular}"
    for p in "" "k" "s" "ks"; do
	echo "\\\framebox{\\includegraphics[width=0.5\\\textwidth]{img/$NOPDF$p.pdf}}"
    done
    echo '\\newpage'
done


echo '\\end{document}'
