import sys
import networkx as nx
import re

from rpp_tools import req_sum, odd_verts


def load_rpp(filename):
    normal_arcs = False
    required_arcs = False

    G = nx.MultiGraph()
    tmp = nx.Graph()

    for line in open(filename):
        if line.startswith(chr(26)):
            continue
        elif line.startswith("COMENTARIO") or line.startswith("DEPOT") or line.startswith("LOBO"):
            continue
        elif line.startswith("LIST_REQ_ARCS"):
            normal_arcs = False
            required_arcs = False
        elif line.startswith("NOMBRE") or line.startswith("NAME"):
            G.graph["name"] = line.split(':')[1].strip()
            continue
        elif line.startswith("VERTICES"):
            G.graph["numverts"] = int(line.split(':')[1].strip())
            continue
        elif line.startswith("LISTA_ARISTAS_REQ") or line.startswith("LIST_REQ_EDGES") or line.startswith("LIST OF ARCS"):
            required_arcs = True
            normal_arcs = False
            continue
        elif line.startswith("LISTA_ARISTAS_NOREQ") or line.startswith("LIST_NOREQ_ARCS") or line.startswith("ARCOS_NOREQ"):
            required_arcs = False
            normal_arcs = True
            continue
        elif required_arcs:
            parse = re.split("[() ,]+", line)
            u = int(parse[1].strip('), '))
            v = int(parse[2].strip('), '))
            tc = int(parse[4].strip('), '))
            if not G.has_edge(u, v):
                tmp.add_edge(u, v, weight=tc)
                G.add_edge(u, v, weight=tc)
        elif normal_arcs:
            parse = re.split("[() ,]+", line)
            u = int(parse[1].strip('), '))
            v = int(parse[2].strip('), '))
            tc = int(parse[4].strip('), '))
            tmp.add_edge(u, v, weight=tc)
#        else:
#            print("Don't know what to do with this line:", line, file=sys.stderr)
    G.graph["dist"] = dict(nx.all_pairs_dijkstra_path_length(tmp))
    return G


def draw(G):
    A = nx.nx_agraph.to_agraph(G)
    A.node_attr['width'] = 0.25
    A.node_attr['height'] = 0.25
    A.node_attr['fixedsize'] = True
    A.node_attr['fontsize'] = 1
    A.node_attr['style'] = "filled"
    A.node_attr['color'] = "black"
    A.graph_attr['dist'] = None
    A.graph_attr['scale'] = 1
    A.draw(path="img/" + G.graph['name'] + ".pdf", prog='neato')


def stats(G):
    n = G.order()
    m = G.number_of_edges()
    b = len(odd_verts(G))
    R = req_sum(G)
    c = nx.number_connected_components(G)
    return (n, m, b, R, c)
