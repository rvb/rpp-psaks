Polynomial-size approximate kernelization scheme for the Rural Postman Problem
==============================================================================

The Rural Postman Problem searches a shortest cycle in a graph
visiting a given subset of edges.  Stated formally:

**Input:** An undirected graph G=(V,E), edge costs ω:E→N, and a subset R⊆E of edges.

**Output:** Find a closed walk W in G that contains all edges in R and minimizes ω(W).

The problem is NP hard and has a known 3/2-approximation.  It is
APX-hard and therefore cannot be approximated arbitrarily well in
polynomial time unless P=NP.  This tool implements a data reduction
algorithm that, for given ε>0, turns any instance I of the Rural
Postman Problem into instance I' so that any α-approximation for
I' can be turned into an (1+ε)α-approximation for I.  Herein,
the size of instance I' is guaranteed to be 2 b+O(c/ε), where
b is the number of vertices with an odd number of incident edges in
R and c is the number of connected components induced by the edges
in R.  The algorithm is presented and evaluated in

- [René van Bevern, Till Fluschnik, and Oxana Yu. Tsidulko. On approximate data reduction for the Rural Postman Problem: Theory and experiments. 2019.](https://arxiv.org/abs/1812.10131)

Note that the current implementation is not optimized for running time
and implemented in Python.  Its aim is to evaluate the shrinking effect and the quality of the
solutions that can be found using this concrete approach.  Towards
competetive running times, one could rewrite it in C++ using the graph
algorithms of, for example, the [LEMON
library](https://lemon.cs.elte.hu/trac/lemon).


Data reduction effect
---------------------

At two examples, we show the data reduction effect for _ε_=0.1.
In both cases, the blue edges are those introduced by data reduction.

The following two pictures show the data reduction effect on a real
snow plowing instance provided to us by the Berlin communal services
(left -- before, right -- after).

<a href="Berlin2.png"><img src="Berlin2.png" width="400px"><a/> <a href="Berlin2k.png"><img src="Berlin2k.png" width="400px"><a/>

The following two pictures show the data reduction effect on instance
UR747 of the [Undirected Rural Postman benchmark set provided by Ángel
Corberán, Isaac Plana, and José María
Sanchis](https://www.uv.es/corberan/instancias.htm) (left -- before, right -- after).

<a href="UR747.png"><img src="UR747.png" width="400px"><a/> <a href="UR747k.png"><img src="UR747k.png" width="400px"><a/>


Usage
-----

Just download a [Zip
archive](https://gitlab.com/rvb/rpp-psaks/repository/archive.zip?ref=master)
or [tar.gz
archive](https://gitlab.com/rvb/rpp-psaks/repository/archive.tar.gz?ref=master)
from Github or clone the project using

```
git clone https://gitlab.com/rvb/rpp-psaks.git
```

To test the program, run

```
python solve_and_draw.py data/[FILENAME]
```

for any of the files in the `data/` directory.  As a result, PDF
drawings in the `img/` directory are created: a drawing of the
original graph, a drawing of the shrunk graph, and approximate
solutions for both graphs.

Acknowledgments
---------------

This tool was created within project 18-501-12031,
"Trade-offs in parameterized data reduction"
of the [Russian Foundation for Basic Research](http://www.rfbr.ru).

----

René van Bevern <rvb@nsu.ru>
