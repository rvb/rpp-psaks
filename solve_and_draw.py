#!/usr/bin/env python3

import sys
import networkx as nx
from rpp_tools import min_connector, fix_weights, flip_balance, odd_verts, bound_T, bound_M, kernelize, req_sum
from rpp_loader import load_rpp, draw, stats
from timeit import default_timer as timer


def best_connect(G, color='red'):
    T = min_connector(G)
    for e in T.edges(data=True):
        G.add_edge(e[2]['origu'], e[2]['origv'], weight=e[2]['weight'],
                   color=color)


def solve_approx(G):
#    fix_weights(G)
    best_connect(G)
    flip_balance(G, odd_verts(G), color='green')
    G.graph['name'] += "s"

    #      instance n1 nr r1 b c w n2 r2 w2 n1n2 nrn2 r1r2 w1w2 w2opt tload tbounds textract tisolated tdecycle tkern

def save_positions(G, pos):
    for v in G.nodes():
        G.nodes[v]["pos"] = "%f,%f!" % (pos[v][0] / 100, pos[v][1] / 100)


print(    "{0:10s} & {1:>4s} & {2:>4s} & {3:>6s} & {4:>4s} & {5:>4s} & {6:>6s} & {7:>4s} & {8:>6s} & {9:>6s} & {10:>4s} & {11:>4s} & {12:>4s} & {13:>4s} & {14:>7s} & {15:>7s} & {16:>7s} & {17:>7s} & {18:>7s} & {19:>7s} & {20:>7s} & {21:2s} \\\\".format(
          "name"   , "n1"    , "nr"    , "r1"    , "b"     , "c"     , "w1"    , "n2"    , "r2"    , "w2"    , "n2n1"   , "n2nr"   , "r2r1"   , "w2w1"   , "tload"  , "tbnd"   , "textr"  , "tisol"  , "tdec"   , "tkern"  , "t"      , "it"), flush=True)


def printline(name, n1, nr, r1, b, c, w, n2, r2, w2, n2n1, n2nr, r2r1, w2w1, tload, tbnd, textr, tisol, tdec, tkern, t, it):
    print("{0:10s} & {1:>4d} & {2:>4d} & {3:>6d} & {4:>4d} & {5:>4d} & {6:>6d} & {7:>4d} & {8:>6d} & {9:>6d} & {10:>4.2f} & {11:>4.2f} & {12:>4.2f} & {13:>4.2f} & {14:>7.3f} & {15:>7.3f} & {16:>7.3f} & {17:>7.3f} & {18:>7.3f} & {19:>7.3f} & {20:>7.3f} & {21:2d} \\\\".format(
          name     ,  n1   ,    nr   ,     r1     , b       , c       , w       , n2      , r2     , w2      , n2n1       , n2nr       , r2r1       , w2w1       , tload      , tbnd       , textr      , tisol      , tdec       , tkern      , t          , it), flush=True)
    

for f in sys.argv[1:]:
    G = load_rpp(f)
    solve_approx(G)
    w1 = req_sum(G)

    pos = nx.nx_pydot.graphviz_layout(G)
    save_positions(G, pos)
    draw(G)

    t1 = timer()
    G = load_rpp(f)

    t2 = timer()
    save_positions(G, pos)

    T = bound_T(G)
    M = bound_M(G)

    (nr, r1, b, R1, c) = stats(G)

    draw(G)
    t3 = timer()
    (it, textract, tisolated, tdecycle, tkern) = kernelize(G, 10, T, M)
    t4 = timer()
    draw(G)
    
    (n2, r2, _, R2, _) = stats(G)

    solve_approx(G)
    draw(G)
    solb = req_sum(G)
    w2 = solb - R2 + R1

    n1 = G.graph['numverts']
    printline(G.graph['name'], G.graph['numverts'], nr, r1, b, c, w1, n2, r2, w2, n2 / n1, n2 / nr, r2 / r1, w2 / w1, t2 - t1, t3 - t2, textract, tisolated, tdecycle, tkern, t4 - t1, it)
